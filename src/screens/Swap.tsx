import { View, Text, SafeAreaView } from 'react-native';
import React from 'react';
import { styles } from '../theme/appTheme';

const Swap = () => {
  return (
    <SafeAreaView style={styles.body}>
      <Text>Soy Swap</Text>
    </SafeAreaView>
  );
};

export default Swap;
